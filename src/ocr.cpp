#include "glog/logging.h"
#include "omp.h"
#include "opencv2/core.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include <chrono>
#include <iomanip>
#include <iostream>
#include <ostream>
#include <vector>

#include <cstring>
#include <fstream>
#include <numeric>

#include <include/config.h>
#include <include/ocr_det.h>
#include <include/ocr_rec.h>
#include <include/JsonLite.h>

using namespace std;
using namespace cv;
using namespace PaddleOCR;

static OCRConfig* pconfig = NULL;

extern "C" __declspec(dllexport) bool LoadModel(DBDetector * *pdet, Classifier * *pcls, CRNNRecognizer * *prec);
__declspec(dllexport) bool LoadModel(DBDetector** pdet, Classifier** pcls, CRNNRecognizer** prec)
{
	OCRConfig config("config.txt");
	config.PrintConfigInfo();
	*pdet = new DBDetector(config.det_model_dir, config.use_gpu, config.gpu_id,
		config.gpu_mem, config.cpu_math_library_num_threads,
		config.use_mkldnn, config.max_side_len, config.det_db_thresh,
		config.det_db_box_thresh, config.det_db_unclip_ratio,
		config.visualize, config.use_tensorrt, config.use_fp16);

	*pcls = nullptr;
	if (config.use_angle_cls == true) {
		*pcls = new Classifier(config.cls_model_dir, config.use_gpu, config.gpu_id,
			config.gpu_mem, config.cpu_math_library_num_threads,
			config.use_mkldnn, config.cls_thresh,
			config.use_tensorrt, config.use_fp16);
	}

	*prec = new CRNNRecognizer(config.rec_model_dir, config.use_gpu, config.gpu_id,
		config.gpu_mem, config.cpu_math_library_num_threads,
		config.use_mkldnn, config.char_list_file,
		config.use_tensorrt, config.use_fp16);
	//std::cout << "00000" << std::endl;
	return (pdet != NULL);
}
/*
1.���ؿ�ѡ���壻
2.���ؽ����ַ���
*/

extern "C" __declspec(dllexport) void Rec(DBDetector * pdet, Classifier * pcls, CRNNRecognizer * prec, char* input, int width, int height, char** result);
__declspec(dllexport) void Rec(DBDetector* pdet, Classifier* pcls, CRNNRecognizer* prec, char* input, int width, int height, char** result)
{
	JsonObjectBuilder builder;
	cv::Mat srcimg(height, width, CV_8UC3, input);
	//std::cout << "00000" << std::endl;

	auto start = std::chrono::system_clock::now();
	std::vector<std::vector<std::vector<int>>> boxes;
	// ���
	(*pdet).Run(srcimg, boxes);
	// ʶ��
	(*prec).Run(boxes, srcimg, pcls, builder);

	JString cs = builder.Build();
	std::cout << cs << std::endl;
	*result = new char[cs.GetLength()+10];
	strcpy(*result, cs);

	auto end = std::chrono::system_clock::now();
	auto duration =
		std::chrono::duration_cast<std::chrono::microseconds>(end - start);
	std::cout << "Cost  "
		<< double(duration.count()) *
		std::chrono::microseconds::period::num /
		std::chrono::microseconds::period::den
		<< "s" << std::endl;
	//return new cv::Mat(srcimg);
}