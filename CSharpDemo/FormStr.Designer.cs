﻿
namespace OCRDemo
{
    partial class FormStr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormStr));
            this.button1 = new System.Windows.Forms.Button();
            this.rt1 = new System.Windows.Forms.RichTextBox();
            this.rt2 = new System.Windows.Forms.RichTextBox();
            this.rt3 = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(348, 39);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 36);
            this.button1.TabIndex = 0;
            this.button1.Text = "处理";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // rt1
            // 
            this.rt1.Location = new System.Drawing.Point(35, 39);
            this.rt1.Name = "rt1";
            this.rt1.Size = new System.Drawing.Size(290, 547);
            this.rt1.TabIndex = 3;
            this.rt1.Text = resources.GetString("rt1.Text");
            // 
            // rt2
            // 
            this.rt2.Location = new System.Drawing.Point(444, 39);
            this.rt2.Name = "rt2";
            this.rt2.Size = new System.Drawing.Size(290, 547);
            this.rt2.TabIndex = 3;
            this.rt2.Text = "";
            // 
            // rt3
            // 
            this.rt3.Location = new System.Drawing.Point(744, 39);
            this.rt3.Name = "rt3";
            this.rt3.Size = new System.Drawing.Size(290, 547);
            this.rt3.TabIndex = 3;
            this.rt3.Text = "";
            // 
            // FormStr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1046, 626);
            this.Controls.Add(this.rt3);
            this.Controls.Add(this.rt2);
            this.Controls.Add(this.rt1);
            this.Controls.Add(this.button1);
            this.Name = "FormStr";
            this.Text = "FormStr";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox rt1;
        private System.Windows.Forms.RichTextBox rt2;
        private System.Windows.Forms.RichTextBox rt3;
    }
}