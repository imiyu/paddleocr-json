﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCRDemo
{
    public partial class FormShow : Form
    {
        public FormShow()
        {
            InitializeComponent();
        }
        public void Show(Image img)
        {
            pictureBox1.Image = img;
        }
    }
}
