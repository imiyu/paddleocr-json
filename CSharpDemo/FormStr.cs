﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OCRDemo
{
    public partial class FormStr : Form
    {
        public FormStr()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OcrJson ocr = JsonHelper.DeserializeJsonToObject<OcrJson>(rt1.Text);
            if (ocr != null && ocr.array != null)
            {
                OcrJson ocr2 = new OcrJson();
                int top1 = 100;
                int top2 = 1000;//图像高度做为最大值
                var yyzz = ocr.array.Find(o => o.str.StartsWith("统一"));//o.str.Contains("营业执照")
                if (yyzz != null)
                {
                    top1 = yyzz.box.top - 30;
                }

                var djjg = ocr.array.Find(o => o.str.Contains("登记机关"));
                if (djjg != null)
                {
                    top2 = djjg.box.top;
                }
                ocr2.array = ocr.array.FindAll(o => (o.box.top >= top1 && o.box.top <= top2 && o.score > 0.7)).ToList();

                string c_id = findstr0(ocr2);//
                string c_name = findstr1(ocr2);//名称
                string c_type = findstr2(ocr2);//类型
                string c_zcxs = findstr3(ocr2);//组成形式
                string c_jyz = findstr4(ocr2);//经营者
                string c_zcrq = findstr5(ocr2);//注册日期
                string c_jycs = findstr6(ocr2);//经营场所
                string c_jyfw = findstr7(ocr2);//经营范围

                string ostr = $"{c_id}\r\n{c_name}\r\n{c_type}\r\n{c_zcxs}\r\n{c_jyz}\r\n{c_zcrq}\r\n{c_jycs}\r\n{c_jyfw}";
                rt3.Text = ostr;

                JsonHelper.SetFormat(true);
                rt2.Text = JsonHelper.SerializeObject(ocr2);

            }

        }
        string findstr0(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.Contains("统一"));
            if (item != null)
            {
                if (item.str.Length > 12)
                {
                    return item.str;
                }
                //文字下方左对齐
                var item1 = ocr.array.Find(o => o.box.top > item.box.top + 20 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left - 30 && o.box.left < item.box.left + 30);
                if (item1 != null)
                {
                    return item1.str;
                }
                else
                {
                    //文字后紧跟
                    item1 = ocr.array.Find(o => o.box.top > item.box.top - 20 && o.box.top < item.box.top + item.box.height && o.box.left > item.box.left + 30);
                    if (item1 != null)
                    {
                        return item1.str;
                    }
                }
            }
            return "";
        }

        string findstr1(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.StartsWith("称"));
            if (item != null)
            {
                if (item.str.Length > 5)
                {
                    return "名" + item.str;
                }
                var item1 = ocr.array.Find(o => o.box.top > item.box.top - 20 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left + 30);
                if (item1 != null)
                {
                    return "名称" + item1.str;
                }
            }
            string restr = "";
            return restr;
        }
        string findstr2(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.StartsWith("型"));
            if (item != null)
            {
                if (item.str.Length > 5)
                {
                    return "类" + item.str;
                }
                var item1 = ocr.array.Find(o => o.box.top > item.box.top - 20 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left + 30);
                if (item1 != null)
                {
                    return "类型" + item1.str;
                }
            }
            string restr = "";
            return restr;
        }
        string findstr3(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.Contains("组成"));
            if (item != null)
            {
                if (item.str.Length > 4)
                    return item.str;
                else
                {
                    var item1 = ocr.array.Find(o => o.box.top > item.box.top - 10 && o.box.top < item.box.top + 50 && o.box.left > item.box.left + item.box.width - 30);
                    if (item1 != null)
                    {
                        return item.str + item1.str;
                    }
                }
            }
            string restr = "";
            return restr;
        }
        string findstr4(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.StartsWith("经营者"));
            if (item == null)
            {
                item = ocr.array.Find(o => o.str.StartsWith("法定代表"));
            }
            if (item != null)
            {
                if (item.str.StartsWith("经营者") && item.str.Length >= 5)
                {
                    return item.str;
                }
                if (item.str.StartsWith("法定代表") && item.str.Length >= 7)
                {
                    return item.str;
                }
                var item1 = ocr.array.Find(o => o.box.top > item.box.top - 20 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left + 30);
                if (item1 != null)
                {
                    return "法人" + item1.str;
                }
            }
            string restr = "";
            return restr;
        }
        string findstr5(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.Contains("注册日期"));
            if (item != null)
            {
                if (item.str.Contains("年"))
                    return item.str;
                var item2 = ocr.array.Find(o => o.box.top > item.box.top - 10 && o.box.top < item.box.top + 50 && o.box.left > item.box.left + item.box.width - 30);
                if (item2 != null)
                {
                    return item.str + item2.str;
                }
            }
            var item1 = ocr.array.Find(o => o.str.Contains("年") && o.str.Contains("月"));
            if (item1 != null)
            {
                return item1.str;
            }
            return "";
        }

        string findstr6(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.StartsWith("经营场所"));
            if (item == null)
            {
                item = ocr.array.Find(o => o.str.StartsWith("住"));
                if (item != null)
                {
                    if (item.str.Length == 1)
                    {
                        item = ocr.array.Find(o => o.str.StartsWith("所"));
                        if (item != null)
                        {
                            if (item.str.Length > 5)
                                return "住" + item.str;
                            var item1 = ocr.array.FindAll(o => o.box.top > item.box.top - 20 && o.box.top < item.box.top + 2 * item.box.height-20 && o.box.left > item.box.left + 30).ToList();
                            if (item1 != null)
                            {
                                string addr = "";
                                foreach (var it in item1)
                                {
                                    addr += it.str;
                                }
                                return "住所" + addr;
                            }
                        }
                    }
                }
            }
            if (item != null)
            {
                if (item.str.Length > 4)
                {
                    //判断是否还有
                    var item2 = ocr.array.FindAll(o => o.box.top > item.box.top - 10 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left + 30).ToList();
                    if (item2 != null)
                    {
                        string addr = "";
                        foreach (var it in item2)
                        {
                            addr += it.str;
                        }

                        return item.str + addr;
                    }
                    return item.str;
                }
                else
                {
                    var item1 = ocr.array.FindAll(o => o.box.top > item.box.top - 10 && o.box.top < item.box.top + 2 * item.box.height && o.box.left > item.box.left + item.box.width - 30).ToList();
                    if (item1 != null)
                    {
                        string addr = "";
                        foreach (var it in item1)
                        {
                            addr += it.str;
                        }

                        return item.str + addr;
                    }
                }
            }
            string restr = "";
            return restr;
        }
        string findstr7(OcrJson ocr)
        {
            var item = ocr.array.Find(o => o.str.Contains("经营范围"));
            if (item != null)
            {
                if (item.str.Length > 4)
                    return item.str;
                else
                {
                    var item1 = ocr.array.FindAll(o => o.box.top > item.box.top - 30 && o.box.left > item.box.left + item.box.width - 30 && o.box.left < item.box.left + item.box.width + 100).ToList();
                    if (item1 != null)
                    {
                        string str = "";
                        foreach (var it in item1)
                        {
                            str += it.str;
                        }
                        return item.str + str;
                    }
                }
            }
            string restr = ocr.array.Find(o => o.str.StartsWith("组成")).str;
            return restr;
        }
    }
}
