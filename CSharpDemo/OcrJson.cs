﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OCRDemo
{
    public class OcrJson
    {
        public List<Array> array { get; set; }
    }

    public class Array
    {
        public Box box { get; set; }
        public string str { get; set; }
        public float score { get; set; }
    }

    public class Box
    {
        public int left { get; set; }
        public int top { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }

}
