# 说明：
本类库是基于PaddleOCR 的简单封装，可以将识别结果以json格式输出，使用到了JsonLite。
方便使用C#等语言直接调用，可自行编译为CPU、GPU版本。

此封装参考了：ch_ccc的文章 [PaddleOCR文字识别C#部署](https://blog.csdn.net/ch_ccc/article/details/113857304)

# 参考PaddleOCR的C++预测编译

## 编译说明
- Windows环境，目前支持基于`Visual Studio 2019 Community`进行编译。

* 当前仅在Windows环境使用过，具体编译方法请参考[Windows下编译教程](./docs/windows_vs2019_build.md)
* 如有问题请参考飞浆官方编译说明。

## C#调用例子效果
<div align="center">
    <img src="./pic/demo.png" width="800">
	<img src="./pic/ShareXDemo.gif" width="800">
</div>
